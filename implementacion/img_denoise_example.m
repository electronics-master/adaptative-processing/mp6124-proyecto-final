%% Initialization
clear
close all
clc
pkg load image

%% Input Image
Img = im2double(rgb2gray(imread('lenna.png')));
M = 0;
V = 0.02;
In = imnoise(Img, 'gaussian', M, V);
figure
subplot(1,2,1), imshow(Img);
title(sprintf('Original Image'));
subplot(1,2,2), imshow(In);
title(sprintf('Noisy Image \n(Gaussian: Mean = %d, Variance = %f)', M, V));
 
%% IQMD Filtering
Out = iqmd_denoise(Img, In);
figure
subplot(1,2,1), imshow(In);
title(sprintf('Noisy Image\n(Gaussian: Mean = %d, Variance = %f)', M, V));
subplot(1,2,2), imshow(Out);
title(sprintf('Output Image'));
