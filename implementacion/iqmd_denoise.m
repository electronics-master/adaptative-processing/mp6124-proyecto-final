function out = iqmd_denoise (orig_img, noise_img)
  max_iters = 1000;
  tol = 1e-4;
  s = size(orig_img)(1);
  out = zeros(s);
  for i = 1:32:s
    part_orig_img = orig_img(i:i+31,:);
    part_noise_img = noise_img(i:i+31,:);
    Rxx = (1/s).*(part_noise_img*part_noise_img');
    Ryx = (1/s).*(part_orig_img*part_noise_img');
    [A, B, total_err, iter] = iqdm (Rxx, Ryx, 16, max_iters, tol);
    T = A*B';
    out(i:i+31,:) = T*part_noise_img;
  endfor
endfunction

