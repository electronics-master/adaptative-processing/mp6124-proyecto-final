m = 8;
n = 6;
r = 4;

R_xx = randn(n, n);
H = eye(m, n);
R_yx = H * R_xx;


B = randn(n, r);


max_iters = 1000;
tol = 1e-4

oldA = zeros(m, r);
oldB = zeros(n, r);
total_err = [];

for i = 1:max_iters
  A = R_yx * B * inv(B' * R_xx * B);
  B = (inv(A' * A) *  A' * R_yx *  inv(R_xx))';

  err = norm(A*B'-oldA*oldB', "fro");

  total_err = [total_err; err];

  if err < tol
    break
  endif

  oldA = A;
  oldB = B;
endfor

figure(1);
h = plot(log(total_err));
xlabel("iteraciones");
ylabel("log(error)");
waitfor(h);
