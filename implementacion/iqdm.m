function [A, B, total_err, i] = iqdm (R_xx, R_yx, r, max_iters, tol)
  n = size(R_xx)(1);
  m = size(R_yx)(1);

  B = randn(n, r);

  oldA = zeros(m, r);
  oldB = zeros(n, r);
  total_err = [];

  for i = 1:max_iters
    A = R_yx * B * pinv(B' * R_xx * B);
    B = (pinv(A' * A) *  A' * R_yx *  pinv(R_xx))';

    err = norm(A*B'-oldA*oldB', "fro");

    total_err = [total_err; err];

    if err < tol
      break
    endif

    oldA = A;
    oldB = B;
  endfor
endfunction
