m = 8;
n = 6;
r = 4;

R_xx = zeros(n, n);
for i = 1:n
  for j = 1:n
    if abs(i-j) < 2
      R_xx(i, j) = randn();
      R_xx(i, j) = randn();
    endif
  endfor
endfor
H = eye(m, n);
R_yx = H * R_xx;

max_iters = 1000;
tol = 1e-4;

[A, B, total_err, i] = iqdm (R_xx, R_yx, r, max_iters, tol);

figure(1);
h = plot(log(total_err));
xlabel("iteraciones");
ylabel("log(error)");
waitfor(h);
